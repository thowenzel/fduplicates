# fduplicates

A command line tool written in Rust to find duplicate files.

The idea: At first fduplicates compares file sizes, then MD5 signatures, and then performs a byte-by-byte comparison for verification.

Open goals (roadmap for version 0.1.0):

* Run fduplicates with arguments
* Compare file sizes -> get all pairs with identical file size
* Compare MD5 signatures for the pairs
* Performs a byte-by-byte comparison if files have same size and same MD5 signature
* Interactive mode
* Export result as *.txt file
